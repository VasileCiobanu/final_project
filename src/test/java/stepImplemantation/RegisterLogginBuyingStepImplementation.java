package stepImplemantation;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import page.HomePage;

import java.util.concurrent.TimeUnit;

public class RegisterLogginBuyingStepImplementation {

    WebDriver driver;
    HomePage homePage;

    @Given("the user accesses main page")
    public void theUserAccessesHomePage() {
        System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get("http://automationpractice.com/index.php");

    }

    @And("clicks on Sign in button")
    public void theUserClicksSignInButton() {
        homePage = new HomePage(driver);
        homePage.clickOnSignIn();
    }

    @And("fills emails address \"petru.ionescu4@mail.ro\"")
    public void userFillsEmailAddress() {
        homePage.fillUserEmailField();
    }

    @And("clicks on the Create account button")
    public void userClicksCreateAccountButton() {
        homePage.clickOnCreateAccount();
    }

    @And("click on Title Mr.")
    public void clickTitle() {
        //homePage = new homePage(driver);
        homePage.clickOnMrsRadioButton();
    }

    @And("Fill First Name \"Petru\"")
    public void fillFirstName() {
        homePage.fillFirstName();
    }

    @And("Fill Last Name \"Ionescu\"")
    public void fillLastName() {
        homePage.fillLastName();
    }

    @And("Click on Email field")
    public void clickEmailField() {
        homePage.clickEmailfield();
    }

    @And("Fill password \"abc123\"")
    public void setUserPassword() {
        homePage.setUserPassword();
    }

    @And("select \"27\" for day of birth")
    public void selectBirthDay() {
        homePage.setBirthDay();
    }

    @And("select \"February\" for mounth of birth")
    public void selectBirthMonth() {
        homePage.setBirthMonth();
    }

    @And("select \"1990\" for year of birth")
    public void selectBirthYear() {
        homePage.setBirthYear();
    }

    @And("Click on Sign up for our newsletter")
    public void checkNewsletter() {
        homePage.checkNewsletter();
    }

    @And("Click on receive special offers from our partenrs")
    public void checkSpecialOffers() {
        homePage.checkSpecialOffer();
    }

    @And("Fill First Name for address \"Ana\"")
    public void fillAdressFirstName() {
        homePage.fillAdressFirstName();
    }

    @And("Fill Last Name for address \"Ionescu\"")
    public void fillAdressLastName() {
        homePage.fillAdressLastName();
    }

    @And("fill company \"ABCDEF\"")
    public void fillCompanyName() {
        homePage.fillCompanyName();
    }

    @And("fill address \"str. Iuliu Maniu nr 32\"")
    public void fillAdressLine1() {
        homePage.fillAdressLine1();
    }

    @And("fill address2 \"str. Mihai Viteazul nr 48\"")
    public void fillAdressLine2() {
        homePage.fillAdressLine2();
    }

    @And("fill City \"Brasov\"")
    public void fillCity() {
        homePage.fillCity();
    }

    @And("select from State dropdown \"Alabama\"")
    public void setUserState() {
        homePage.setUserState();
    }

    @And("fill zip_postal \"12345\"")
    public void fillZipCode() {
        homePage.fillZipCode();
    }

    @And("select country dropdown \"United States\"")
    public void setUserCountry() {
        homePage.setUserCountry();
    }

    @And("Fill Adittional information with \"hello word\"")
    public void fillAdittionalInfo() {
        homePage.fillAdittionalInfo();
    }

    @And("Fill Home phone \"0268256489\"")
    public void fillHomePhone() {
        homePage.fillHomePhone();
    }

    @And("Fill Mobile phone \"0781234569\"")
    public void fillMobilePhone() {
        homePage.fillMobilePhone();
    }

    @And("Fill assign an address alias for feature references \"Brasov\"")
    public void fillAddresssAlias() {
        homePage.fillAddresssAlias();
    }

    @When("click on Register Button")
    public void userClicksRegisterButton() {
        homePage.userClicksRegisterButton();
    }

    @Then("Account should be successfully created")
    public void checkAccountCreation() {
        //homePage = new homePage(driver);
        homePage.checkAccountCreation();
    }

    //    Login process steps
    @And("Fill login email \"petru.ionescu@mail.ro\"")
    public void userFillLoginEmail() {
        homePage.fillRegisteredEmail();
    }

    @And("Fill login password \"abc123\"")
    public void userFillLoginPassword() {
        homePage.fillRegisteredPassword();
    }

    @When("User clicks on sign in button")
    public void userClicksSignInButton() {
        homePage.clickSignInButton();
    }

    @Then("User should access personal account")
    public void userAccessAccount() {
        homePage.checkAccountCreation();
    }

    //    Buying process
    @And("User clicks on Dresses menu link")
    public void accessDressesProducts() {
        homePage.accessDressesProducts();
    }

    @And("User clicks on discounted Printed Summer Dress")
    public void selectPrintedSummerDress() {
        homePage.selectPrintedSummerDress();
    }

    @And("User select size M")
    public void selectSizeM() {
        homePage.setSize();
    }

    @And("User select color blue")
    public void selectColorBlue() {
        homePage.setColorBlue();
    }

    @And("User add product to cart")
    public void addProductToCart() {
        homePage.addToCart();
    }

    @And("User continues shopping")
    public void continueShopping() {
        homePage.continueShopping();
    }

    @And("User clicks on Women menu link")
    public void accessWomenProducts() {
        homePage.accessWomensProducts();
    }

    @And("User clicks on Blouse products")
    public void selectBlouse() {
        homePage.selectBlouse();
    }

    @And("User select color white")
    public void selectColorWhite() {
        homePage.setColorWhite();
    }

    @And("Click proceed to Checkout button")
    public void proceedToCheckout() {
        homePage.proceedToCheckout();
    }

    @And("Click proceed to Checkout button from cart")
    public void proceedToCheckoutFromCart() {
        homePage.proceedToCheckoutFromCart();
    }

    @And("Check agree terms of service")
    public void checkTermsOfServices() {
        homePage.checkAgreeTerms();
    }

    @And("Click on bank wire payment")
    public void selecBankWirePay() {
        homePage.selecBankWirePay();
    }

    @When("User clicks I confirm my order")
    public void confirmOrder() {
        homePage.proceedToCheckoutFromCart();
    }

    @Then("User should see order confirmation page")
    public void confirmationOrder() {
        homePage.confirmationOrder();

    }
}
