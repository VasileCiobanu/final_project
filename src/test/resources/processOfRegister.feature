Feature: Register new customer

  Scenario: Register new customer - positive scenario
    Given the user accesses main page
    And clicks on Sign in button
    And fills emails address "petru.ionescu4@mail.ro"
    And clicks on the Create account button
    And click on Title Mr.
    And Fill First Name "Petru"
    And Fill Last Name "Ionescu"
    And Click on Email field
    And Fill password "abc123"
    And select "27" for day of birth
    And select "February" for mounth of birth
    And select "1990" for year of birth
    And Click on Sign up for our newsletter
    And Click on receive special offers from our partenrs
    And Fill First Name for address "Ana"
    And Fill Last Name for address "Ionescu"
    And fill company "ABCDEF"
    And fill address "str. Iuliu Maniu nr 32"
    And fill address2 "str. Mihai Viteazul nr 48"
    And fill City "Brasov"
    And select from State dropdown "Alabama"
    And fill zip_postal "12345"
    And select country dropdown "United States"
    And Fill Adittional information with "hello word"
    And Fill Home phone "0268256489"
    And Fill Mobile phone "0781234569"
    And Fill assign an address alias for feature references "Brasov"
    When click on Register Button
    Then  Account should be successfully created
