Feature: User wants to buy products from the online shop

  Scenario: Customer buys products as logged user - positive scenario
    Given the user accesses main page
    And clicks on Sign in button
    And Fill login email "petru.ionescu@mail.ro"
    And Fill login password "abc123"
    And User clicks on sign in button
    And User clicks on Dresses menu link
    And User clicks on discounted Printed Summer Dress
    And User select size M
    And User select color blue
    And User add product to cart
    And User continues shopping
    And User clicks on Women menu link
    And User clicks on Blouse products
    And User select size M
    And User select color white
    And User add product to cart
    And Click proceed to Checkout button
    And Click proceed to Checkout button from cart
    And Click proceed to Checkout button from cart
    And Check agree terms of service
    And Click proceed to Checkout button from cart
    And Click on bank wire payment
    When User clicks I confirm my order
    Then User should see order confirmation page