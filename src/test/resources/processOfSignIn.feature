Feature: User want to login

  Scenario: Login as customer - positive scenario
    Given the user accesses main page
    And clicks on Sign in button
    And  Fill login email "petru.ionescu@mail.ro"
    And  Fill login password "abc123"
    When User clicks on sign in button
    Then User should access personal account