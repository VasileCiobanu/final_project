package page;

import lombok.Getter;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

@Getter
public class HomePage {

    WebDriver driver;

    public HomePage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);

    }

    private String URL = "http://automationpractice.com/index.php";
    private String title = "My Store";

    @FindBy(xpath = "//*[@id=\"header\"]/div[2]/div/div/nav/div[1]/a")
    private WebElement signIn;
    @FindBy(id = "email_create")
    private WebElement createEmailField;
    @FindBy(id = "SubmitCreate")
    private WebElement createAccountButton;
    @FindBy(id = "email")
    private WebElement registeredEmailField;
    @FindBy(id = "passwd")
    private WebElement registeredPasswordField;
    @FindBy(id = "SubmitLogin")
    private WebElement signInButton;
    @FindBy(id = "id_gender1")
    private WebElement mrsGenderRadioField;
    @FindBy(id = "customer_firstname")
    private WebElement customerFirstNameField;
    @FindBy(id = "customer_lastname")
    private WebElement customerLastNameField;
    @FindBy(id = "email")
    private WebElement userEmail;
    @FindBy(id = "passwd")
    private WebElement userPassword;
    @FindBy(id = "days")
    private WebElement birthDay;
    @FindBy(id = "months")
    private WebElement birthMonth;
    @FindBy(id = "years")
    private WebElement birthYear;
    @FindBy(id = "newsletter")
    private WebElement newsletterCheckBox;
    @FindBy(id = "optin")
    private WebElement receiveOffersCheckox;
    @FindBy(id = "firstname")
    private WebElement userAddressFirstName;
    @FindBy(id = "lastname")
    private WebElement userAddressLastName;
    @FindBy(id = "company")
    private WebElement userCompany;
    @FindBy(id = "address1")
    private WebElement userAddress1;
    @FindBy(id = "address2")
    private WebElement userAddress2;
    @FindBy(id = "city")
    private WebElement userCity;
    @FindBy(id = "id_state")
    private WebElement userState;
    @FindBy(id = "postcode")
    private WebElement userPostCode;
    @FindBy(id = "id_country")
    private WebElement userCountry;
    @FindBy(id = "other")
    private WebElement additionalInformation;
    @FindBy(id = "phone")
    private WebElement userHomePhone;
    @FindBy(id = "phone_mobile")
    private WebElement userMobilePhone;
    @FindBy(id = "alias")
    private WebElement userAliasAddress;
    @FindBy(id = "submitAccount")
    private WebElement registrationButton;
    @FindBy(id = "my-account")
    private WebElement bodyId;
    @FindBy(xpath = "//*[@id=\"block_top_menu\"]/ul/li[2]/a")
    private WebElement dressesLink;
    @FindBy(xpath = "//*[@id=\"center_column\"]/ul/li[3]/div/div[2]/h5/a")
    private WebElement printedSummerDress;
    @FindBy(id = "group_1")
    private WebElement sizeDropdown;
    @FindBy(id = "color_14")
    private WebElement colorBlue;
    @FindBy(css = "#add_to_cart > button")
    private WebElement cartButton;
    @FindBy(xpath = "//*[@id=\"layer_cart\"]/div[1]/div[2]/div[4]/span")
    private WebElement continueShoppingButton;
    @FindBy(xpath = "//*[@id=\"block_top_menu\"]/ul/li[1]/a")
    private WebElement womenProducts;
    @FindBy(xpath = "//*[@id=\"center_column\"]/ul/li[2]/div/div[2]/h5/a")
    private WebElement blouseProduct;
    @FindBy(id = "color_8")
    private WebElement colorWhite;
    @FindBy(css = "a[title*='Proceed to checkout']")
    private WebElement proceedToCheckoutButton;
    @FindBy(id = "uniform-cgv")
    private WebElement agreeTerms;
    @FindBy(css = "p.cart_navigation .button-medium")
    private WebElement proceedToCheckoutCartButton;
    @FindBy(css = "a.bankwire")
    private WebElement bankWirePay;


    public void clickOnSignIn() {
        getSignIn().click();
    }

    public void clickOnMrsRadioButton() {
        getMrsGenderRadioField().click();
    }

    public void fillFirstName() {
        getCustomerFirstNameField().sendKeys("Petru");
    }

    public void fillLastName() {
        getCustomerLastNameField().sendKeys("Ionescu");
    }

    public void clickEmailfield() {
        getUserEmail().click();
    }

    public void setUserPassword() {
        getUserPassword().sendKeys("abc123");
    }

    public void setBirthDay() {
        new Select(getBirthDay()).selectByIndex(27);
    }

    public void setBirthMonth() {
        new Select(getBirthMonth()).selectByIndex(2);
    }

    public void setBirthYear() {
        new Select(getBirthYear()).selectByIndex(32);
    }

    public void checkNewsletter() {
        getNewsletterCheckBox().click();
    }

    public void checkSpecialOffer() {
        getReceiveOffersCheckox().click();
    }

    public void fillAdressFirstName() {
        getUserAddressFirstName().clear();
        getUserAddressFirstName().sendKeys("Ana");
    }

    public void fillAdressLastName() {
        getUserAddressLastName().clear();
        getUserAddressLastName().sendKeys("Ionescu");
    }

    public void fillCompanyName() {
        getUserCompany().sendKeys("ABCDE");
    }

    public void fillAdressLine1() {
        getUserAddress1().sendKeys("str. Iuliu Maniu nr 32");
    }

    public void fillAdressLine2() {
        getUserAddress2().sendKeys("str. Mihai Viteazul nr 48");
    }

    public void fillCity() {
        getUserCity().sendKeys("Brasov");
    }

    public void setUserState() {
        new Select(getUserState()).selectByIndex(1);
    }

    public void fillZipCode() {
        getUserPostCode().sendKeys("12345");
    }

    public void setUserCountry() {
        new Select(getUserCountry()).selectByIndex(1);
    }

    public void fillAdittionalInfo() {
        getAdditionalInformation().sendKeys("hello world");
    }

    public void fillHomePhone() {
        getUserHomePhone().sendKeys("0268256489");
    }

    public void fillMobilePhone() {
        getUserMobilePhone().sendKeys("0781234569");
    }

    public void fillAddresssAlias() {
        getUserAliasAddress().clear();
        getUserAliasAddress().sendKeys("Brasov");
    }

    public void userClicksRegisterButton() {
        getRegistrationButton().click();
    }

    public void clickOnCreateAccount() {
        getCreateAccountButton().click();
    }

    public void fillUserEmailField() {
        getCreateEmailField().sendKeys("petru.ionescu4@mail.ro");
    }

    public void fillRegisteredEmail() {
        getRegisteredEmailField().sendKeys("petru.ionescu@mail.ro");
    }

    public void fillRegisteredPassword() {
        getRegisteredPasswordField().sendKeys("abc123");
    }

    public void clickSignInButton() {
        getSignInButton().click();
    }

    public void checkAccountCreation() {
        String bodyId = driver.findElement(By.tagName("body")).getAttribute("id");
        Assert.assertEquals("my-account", bodyId);
    }

    public void accessDressesProducts() {
        getDressesLink().click();
    }

    public void selectPrintedSummerDress() {
        getPrintedSummerDress().click();
    }

    public void setSize() {
        new Select(getSizeDropdown()).selectByIndex(1);
    }

    public void setColorBlue() {
        getColorBlue().click();
    }

    public void addToCart() {
        getCartButton().click();
    }

    public void continueShopping() {
        getContinueShoppingButton().click();
    }

    public void accessWomensProducts() {
        getWomenProducts().click();
    }

    public void selectBlouse() {
        getBlouseProduct().click();
    }

    public void setColorWhite() {
        getColorWhite().click();
    }

    public void proceedToCheckout() {
//        WebDriverWait wait = new WebDriverWait(driver, 20);
//        wait.until(ExpectedConditions.elementToBeClickable(getProceedToCheckoutButton())).click();
        getProceedToCheckoutButton().click();
    }

    public void proceedToCheckoutFromCart() {
        getProceedToCheckoutCartButton().click();
    }

    public void checkAgreeTerms() {
        getAgreeTerms().click();
    }

    public void selecBankWirePay() {
        getBankWirePay().click();
    }

    public void confirmationOrder() {
        String bodyId = driver.findElement(By.tagName("body")).getAttribute("id");
        Assert.assertEquals("order-confirmation", bodyId);
    }
}